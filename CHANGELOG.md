# [3.0.0](https://gitlab.com/to-be-continuous/bash/compare/2.1.1...3.0.0) (2022-08-05)


### Features

* adaptive pipeline ([543d03a](https://gitlab.com/to-be-continuous/bash/commit/543d03aab8471bf722117c14d7c0a084107fb020))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

## [2.1.1](https://gitlab.com/to-be-continuous/bash/compare/2.1.0...2.1.1) (2022-05-06)


### Bug Fixes

* **rules:** implement positive match rule for shellcheck ([0aa727d](https://gitlab.com/to-be-continuous/bash/commit/0aa727d19e0ad243564ace568e885a3a15e689f9))

# [2.1.0](https://gitlab.com/to-be-continuous/bash/compare/2.0.1...2.1.0) (2022-05-01)


### Features

* configurable tracking image ([d204ccc](https://gitlab.com/to-be-continuous/bash/commit/d204cccb77c7c257d7655d2df962b30cbbd07511))

## [2.0.1](https://gitlab.com/to-be-continuous/bash/compare/2.0.0...2.0.1) (2021-10-04)


### Bug Fixes

* update tag version in script ([a26cb1d](https://gitlab.com/to-be-continuous/bash/commit/a26cb1d3399ff14124f7e57707e83ab1e55a6118))

## [2.0.0](https://gitlab.com/to-be-continuous/bash/compare/1.0.0...2.0.0) (2021-09-02)

### Features

* Change boolean variable behaviour ([803eac3](https://gitlab.com/to-be-continuous/bash/commit/803eac3ddd20d039845836105a721191cdba7f21))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## 1.0.0 (2021-06-10)

### Features

* initial release ([a925c72](https://gitlab.com/to-be-continuous/bash/commit/a925c722df2df4d2eb986b5eb74c57652b70e96c))
