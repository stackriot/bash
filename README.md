# GitLab CI template for bash script validation

This project implements a generic GitLab CI template for bash script validation.

## Usage

In order to include this template in your project, add the following to your `.gitlab-ci.yml`:

```yaml
include:
  - project: 'to-be-continuous/bash'
    ref: '3.0.0'
    file: '/templates/gitlab-ci-bash.yml'
```

## Jobs

### `bash-shellcheck` job

This job performs a static analysis of your shell scripts using [ShellCheck](https://github.com/koalaman/shellcheck).

| Name                    | description                            | default value     |
| ----------------------- | -------------------------------------- | ----------------- |
| `BASH_SHELLCHECK_DISABLED` | Set to `true` to disable ShellCheck                                                | _none_ (enabled) |
| `BASH_SHELLCHECK_IMAGE` | The Docker image used to run [ShellCheck](https://github.com/koalaman/shellcheck) | `koalaman/shellcheck-alpine:stable` |
| `BASH_SHELLCHECK_FILES` | Shell file(s) pattern to analyse                                                  | `**/*.sh`            |
| `BASH_SHELLCHECK_OPTS`  | ShellCheck [options](https://github.com/koalaman/shellcheck/blob/master/shellcheck.1.md) | _none_ |

### `bash-bats` job

This job performs unit tests based on [Bats](https://github.com/bats-core/bats-core) (Bash Automated Testing System).

The job uses the following variables:

| Name                    | description                            | default value     |
| ----------------------- | -------------------------------------- | ----------------- |
| `BASH_BATS_ENABLED`     | Set to `true` to enable bats tests                                                    | _none_ (disabled) |
| `BASH_BATS_IMAGE`       | The Docker image used to run [Bats](https://github.com/bats-core/bats-core) | `bats/bats:1.2.1` |
| `BASH_BATS_TESTS`       | The path to a Bats test file, or the path to a directory containing Bats test files | `tests`           |
| `BASH_BATS_OPTS`        | Bats [options](https://github.com/bats-core/bats-core#usage)                | `--formatter junit --output reports` |
| `BASH_BATS_LIBRARIES`   | Coma separated list of Bats [libraries and add-ons](https://github.com/bats-core/bats-core#libraries-and-add-ons) (formatted as `lib_name_1@archive_url_1 lib_name_2@archive_url_2 ...`) | _none_ |


#### How to manage libraries and add-ons

The Docker image used only contains [bats-core](https://github.com/bats-core/bats-core).
If you wish to used Bats [libraries and add-ons](https://github.com/bats-core/bats-core#libraries-and-add-ons), you have
two options: 

1. either use the Git submodule technique whenever possible,
2. or configure the list of libraries to load prior to running tests with the `BASH_BATS_LIBRARIES` variable. 
   The loaded libraries will then be available in `$BATS_LIBRARIES_DIR/$lib_name`.

Example of a project using [bats-support](https://github.com/bats-core/bats-support) and [bats-assert](https://github.com/bats-core/bats-assert):

* `BASH_BATS_LIBRARIES: "bats-support@https://github.com/bats-core/bats-support/archive/v0.3.0.zip bats-assert@https://github.com/bats-core/bats-assert/archive/v2.0.0.zip"`
* in test scripts, libraries can be loaded as follows:
```bash
#!/usr/bin/env bats
load "$BATS_LIBRARIES_DIR/bats-support/load.bash"
load "$BATS_LIBRARIES_DIR/bats-assert/load.bash"

@test "test something" {
    run echo "Hello there"
    assert_line "Hello there"
}
```
